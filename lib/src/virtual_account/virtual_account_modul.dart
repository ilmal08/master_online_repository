import 'package:flutter/material.dart';
import 'package:modularized_virtual_account_rnd/virtual_account_modularized.dart'
    as va;

class ModularVirtualAccount extends StatefulWidget {
  ModularVirtualAccount({Key? key}) : super(key: key);

  @override
  State<ModularVirtualAccount> createState() => _ModularVirtualAccountState();
}

class _ModularVirtualAccountState extends State<ModularVirtualAccount> {
  @override
  Widget build(BuildContext context) {
    return va.VirtualAccountApp();
  }
}
